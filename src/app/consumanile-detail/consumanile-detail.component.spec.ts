import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumanileDetailComponent } from './consumanile-detail.component';

describe('ConsumanileDetailComponent', () => {
  let component: ConsumanileDetailComponent;
  let fixture: ComponentFixture<ConsumanileDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsumanileDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumanileDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
