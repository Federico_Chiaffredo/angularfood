import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsumabileInsertComponent } from './consumabile-insert/consumabile-insert.component';
import { ConsumabileListComponent } from './consumabile-list/consumabile-list.component';
import { ConsumabileUpdateComponent } from './consumabile-update/consumabile-update.component';
import { ConsumanileDetailComponent } from './consumanile-detail/consumanile-detail.component';

const routes: Routes = [
  {path:"", redirectTo:"consumabile/list",pathMatch:"full"},
  {path:"consumabile/insert", component: ConsumabileInsertComponent},
  {path:"consumabile/update", component: ConsumabileUpdateComponent},
  {path:"consumabile/list", component: ConsumabileListComponent},
  {path:"consumabile/detail/:codice", component: ConsumanileDetailComponent}
];

@NgModule({ 
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
