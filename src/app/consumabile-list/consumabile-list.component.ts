import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona';

@Component({
  selector: 'app-consumabile-list',
  templateUrl: './consumabile-list.component.html',
  styleUrls: ['./consumabile-list.component.css']
})
export class ConsumabileListComponent implements OnInit {

  //PROVE FUNZIONI E VARIABILI

 dimmiCiao(temp){
   console.log(temp);
 }

 sommaNumeri(var1:number,var2:number) :number {
  return var1+var2;
 }



 //DICHIARARE UNA VARIABILE
numero1:number=34;

//PER DICHIARARE ARRAY
elenco: any=[];

//PER DICHIARARE OGGETTI

persona:any={};

//POSSO DICHIARARE ATTRIBUTI DA ZERO e sul momento


//DOPO AVER DICHIARATO LA CLASSE POSSO CHIAMARLA COME TIPO. DEVO PERò RISPETTARE GLI ATTRIBUTI RICHIESTI DALLA CLASSE.
// CON LOGGETTO HO TOTALE LIBERTà

persona1:Persona=new Persona();



  constructor() { 
  
  }


  //QUESTO SAREBBE ANALOGO DI DOCUMENT.READY
  ngOnInit(): void {
    
  }

}
