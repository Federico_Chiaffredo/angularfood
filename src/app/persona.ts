export class Persona{
    nome:String|undefined;
    cognome:String|undefined;
    eta: String|number|undefined;
    }

//POSSO dichiarare diverse possibilità di tipo per l'input
//i tipi undefined sono OPZIONALI.
//Attributi vengono inizializzati soltanto quando hanno contenuto al loro interno 
//(classe diventa tipo un interfaccia in cui devo rispettare le direttive, che però sono opzionali)
//classe è tipo bordo olte il quale non posso uscire, ma posso stare su un suo sottoinsieme


    //EXPORT METTE L'OGGETTO A DISPOSIZIONE DI TUTTE LE STRUTTURE
    //Quando però dichiarerò qualcosa come Persona posso solo usare come attributi nome e cognome (non posso settare l'età per dire)
    //Usando oggetti invece ho totale libertà per creare tutti gli attributi che voglio sul momento