import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConsumanileDetailComponent } from './consumanile-detail/consumanile-detail.component';
import { ConsumabileUpdateComponent } from './consumabile-update/consumabile-update.component';
import { ConsumabileInsertComponent } from './consumabile-insert/consumabile-insert.component';
import { ConsumabileListComponent } from './consumabile-list/consumabile-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ConsumanileDetailComponent,
    ConsumabileUpdateComponent,
    ConsumabileInsertComponent,
    ConsumabileListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
